import argparse
from utils.Singleton import Singleton


class Params(metaclass=Singleton):
    def __init__(self, *args, **kwargs):
        parser = argparse.ArgumentParser(
            prog="EvilTwin",
            description="Process arguments to start evil twin")

        parser.add_argument('ssid', type=str, help="SSID to create")
        parser.add_argument('-passwd', '--password', type=str, help="Password for the AP")
        parser.add_argument('-c', '--connect', help="Connect a host", action="store_true")
        parser.add_argument('-v', '--verbose', help="Acivates Log", action="store_true")

        args = parser.parse_args()
        self.args = args

    def get(self, arg_name=None):
        return self.args
