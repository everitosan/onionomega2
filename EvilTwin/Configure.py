import json
import Logger
from subprocess import call
from BackUp import back_conf_file, restore_conf_file


def make_conf(ssid, passwd=None):

    back_conf_file()

    __set_param('ssid', ssid)
    if passwd is None:
        Logger.warn("No password detected, that means the AP will be open")
        __set_param('encryption', 'open')
    else:
        __set_param('key', passwd)
        __set_param('encryption', 'psk2')

    __commit_conf()


def undo_conf():
    conf = restore_conf_file()
    __set_param('encryption', conf['encryption'])
    __set_param('ssid', conf['ssid'])
    __set_param('key', conf['key'])
    __commit_conf()
    Logger.log("Configuration restored ...")
    Logger.log("Bye bye")


def __set_param(param, value):
    Logger.log("Setting {} to: {}".format(param, value))
    cmd = "uci set wireless.@wifi-iface\[0\].{}={}".format(param, value)
    call(cmd, shell=True)


def __commit_conf():
    Logger.warn("It may take a wile, restarting network ...")
    call("uci commit wireless", shell=True)
    call("/etc/init.d/network restart", shell=True)
