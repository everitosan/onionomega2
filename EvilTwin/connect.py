from subprocess import call
import Logger


def start_connect():
    """
    Connects to a new access point with the help of wifisetup
    """
    route = "/usr/bin/wifisetup"
    try:
        Logger.log("Starting WifiSetup...")
        call(route)
        Logger.log("...WifiSetup finished")
    except FileNotFoundError:
        Logger.error(
            "wifisetup not found, make shure the bin is under {}".format(route))
