from arguments import Params
import sys


def log(msg):
    __print("[·] {}".format(msg), '\033[92m')


def warn(msg):
    __print("[!] {}".format(msg), '\033[93m')


def verbose(msg):
    __print("[^] {}".format(msg), '\033[95m')


def error(msg):
    __print("[x] {}".format(msg), '\033[91m')
    __print("--x--Finishing--x--", '\033[91m')
    sys.exit(1)


def __print(msg, color):
    if Params().get().verbose:
        print(color+msg+'\033[0m')
