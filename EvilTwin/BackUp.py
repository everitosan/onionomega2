import json
import Logger
from subprocess import check_output


BACKUP_NAME = "conf.tmp"


def back_conf_file():
    """
    Gets actual configuration and save it in a temporal file
    """
    CONF = {
        "ssid": __get_stored_param('ssid'),
        "key": __get_stored_param('key'),
        "encryption": __get_stored_param('encryption'),
    }

    Logger.log("Saving known configuration...")
    CONF_str = json.dumps(CONF)
    with open(BACKUP_NAME, "w+") as backup:
        backup.write(CONF_str)
    Logger.verbose(CONF_str)


def restore_conf_file():
    """
    Restore configuration from backup file and returns it as a json
    """
    CONF = {}
    with open(BACKUP_NAME, "r") as backup:
        CONF_str = backup.read()
        CONF = json.loads(CONF_str)
        Logger.verbose(CONF_str)

    return CONF


def __get_stored_param(param):
    line = check_output(
        "uci show wireless | grep 'wireless.@wifi-iface\[0\].{}'".format(param),
        shell=True)
    return line.decode('ascii').split("'")[1]
