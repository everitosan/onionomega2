from arguments import Params
from connect import start_connect
from Configure import make_conf, undo_conf
import os


def main():
    """
    [··] Main entrance
        - Get parameters and start process
    """
    params = Params()
    ssid = params.get().ssid
    passwd = params.get().password
    should_connect = params.get().connect

    if should_connect:
        start_connect()

    try:
        make_conf(ssid, passwd)
        while True:
            pass
    except KeyboardInterrupt:
        undo_conf()


if __name__ == "__main__":
    main()
